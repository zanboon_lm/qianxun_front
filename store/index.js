import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		//接口域名
		URL:"http://qianxun.zanboon.com:9105/",
        //token
		token:"",
        avatarUrl: '',
		nickName: '',
		//商家token
		merchantToken:"",
        isAuthorization: false,
		//商家默认ID
		merchantDefaultID:1,
		orderId:null
	},
	getters:{
		getMerchantToken(state){
			return state.merchantToken;
		}
	},
	mutations: {
		updataToken(state,token){
			state.token = token;
			state.isAuthorization = true;
		},
		getNameImgurl(state,info){
			state.nickName = info.nickName;
			state.avatarUrl = info.avatarUrl;
		},
		updataMerchantToken(state,merchantToken){
			state.merchantToken = merchantToken;
		},
		getOrderId(state,orderId){
			state.orderId = orderId;
		}
	},
	actions: {
		  
	}
})

export default store
